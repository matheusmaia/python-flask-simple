FROM python:3.9

ARG YOUR_ENV

WORKDIR /python_flask_simple
COPY pyproject.toml poetry.lock /python_flask_simple/

RUN pip install --user poetry
ENV PATH="${PATH}:/root/.local/bin"
RUN poetry install && poetry update

RUN poetry export --without-hashes > requirements.txt
RUN pip install -r requirements.txt

COPY ./python_flask_simple ./python_flask_simple

CMD [ "python", "./python_flask_simple/app.py" ]